/****************************************
 * Simple graphics visualization
 ****************************************/
#ifndef GRPAPH_H
#define GRPAPH_H

#include <stdint.h>

#ifdef __DragonFly__
#define GRAPHICS
#define INITGRAPH(_w, _h, _path) initgraph(_w, _h, _path)
#define CLOSEGRAPH() closegraph()
#define WRITE_GRAPH_MAP() write_graph_map()
void initgraph(uint16_t, uint16_t, char const *path);
void closegraph(void);
void write_graph_map(void);
#else
#undef GRAPHICS
#define INITGRAPH(_w, _h, _path)
#define CLOSEGRAPH()
#define WRITE_GRAPH_MAP()
#endif

#endif // GRAPH_H
// vim: set et sw=4 ts=4:
