#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <png.h>

#include "assert.h"
#include "dbg.h"

#include "graph.h"
#ifdef GRAPHICS
#include "yrmem-priv.h"

/****************************************
 * One pixel
 ****************************************/
typedef struct {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} pixel_t;

/****************************************
 * "syntax" colors
 ****************************************/
static pixel_t UNUSED_DATA      = {0x30, 0x30, 0x30};
static pixel_t UNUSED_HEADER    = {0x00, 0x00, 0x8f};
static pixel_t USED_DATA        = {0x00, 0x8f, 0x00};
static pixel_t USED_HEADER      = {0x8f, 0x00, 0x00};
/****************************************
 * Picture as whole
 ****************************************/
typedef struct {
    pixel_t *pixels;
    uint16_t  width;
    uint16_t  height;
} bitmap_t;

/****************************************
 * The main bitmap
 ****************************************/
static bitmap_t img;
#define MAX_PATH_LEN  1024
static char img_path[MAX_PATH_LEN - 20];
static uint16_t img_idx;
static char img_filename[MAX_PATH_LEN + 1];

/****************************************
 * Access to one pixel
 ****************************************/
static pixel_t *pixel_at(uint16_t x, uint16_t y);
static pixel_t *pixel_at(uint16_t x, uint16_t y) {
    return (img.pixels + x + y * img.width);
}

/****************************************
 * Initialize the graphics
 ****************************************/
void initgraph(uint16_t width, uint16_t height, char const *path) {
    assert(path);

    img.width = width;
    img.height = height;
    img.pixels = calloc(width * height, sizeof(pixel_t));
    assert(img.pixels);

    strncpy(img_path, path, MAX_PATH_LEN + 1);
    img_idx = 0;
}

/****************************************
 * Save image to file
 ****************************************/
static void save_png(void);
static void save_png(void) {
    FILE *fp = fopen(img_filename, "wb");

    if (!fp) {
        dbg("Can't open %s\n", img_filename);
        return;
    }
    do {
        // internal PNG structures
        png_structp png_ptr;
        png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
                NULL, NULL, NULL);
        if (!png_ptr) {
            dbg("Create PNG write struct error\n");
            break;
        }
        png_infop info_ptr;
        info_ptr = png_create_info_struct(png_ptr);
        if (!info_ptr) {
            dbg("Crate PNG info struct error\n");
            png_destroy_write_struct(&png_ptr, &info_ptr);
            break;
        }
        // set error handler
        if (setjmp(png_jmpbuf(png_ptr))) {
            png_destroy_write_struct(&png_ptr, &info_ptr);
            break;
        }
        // set image attributes
        png_set_IHDR(png_ptr, info_ptr, img.width, img.height,
                8 /* depth */,
                PNG_COLOR_TYPE_RGB,
                PNG_INTERLACE_NONE,
                PNG_COMPRESSION_TYPE_DEFAULT,
                PNG_FILTER_TYPE_DEFAULT);

        // create the PNG rows
        png_byte **rows = png_malloc(png_ptr,
                        img.height * sizeof(png_byte *));
        for (uint y = 0; y < img.height; ++y) {
            png_byte *row = png_malloc(png_ptr,
                    sizeof(uint8_t) * img.width * 3 /* pixel size */);
            rows[y] = row;
            for (uint x = 0; x < img.width; ++x) {
                pixel_t *pixel = pixel_at(x, y);
                *row++ = pixel->red;
                *row++ = pixel->green;
                *row++ = pixel->blue;
            }
        }
        // write
        png_init_io(png_ptr, fp);
        png_set_rows(png_ptr, info_ptr, rows);
        png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);
        // free mem
        for (uint y = 0; y < img.height; ++y) {
            png_free(png_ptr, rows[y]);
        }
        png_free(png_ptr, rows);
    } while(0);
    fclose(fp);
}


/****************************************
 * Write megapixel 10x10 in the 640x640 image
 ****************************************/
#define SCALE_Y 10
#define SCALE_X 10
static void write_megapixel(uint idx, pixel_t const *pix);
static void write_megapixel(uint idx, pixel_t const *pix) {
    uint big_x = idx % (img.width / SCALE_X);
    uint big_y = idx / (img.height / SCALE_Y);

    pixel_t *bmp_ptr = img.pixels + big_y * SCALE_Y * img.width
                       + big_x * SCALE_X;
    for (uint y = 0; y < SCALE_Y; ++y) {
        for (uint x = 0; x < SCALE_X; ++x) {
            bmp_ptr[x] = *pix;
        }
        bmp_ptr += img.width;
    }
}

/****************************************
 * Write the memory map to the PNG
 ****************************************/
void write_graph_map(void) {
    // get heap
    mhdr_t const *heap = priv_yrmem_get_heap();
    midx_t cur = 0;
    uint mega_idx = 0;
    do {
        midx_t next = heap[cur].next;
        uint8_t free_flg = heap[cur].free_flg;
        if (free_flg) {
            write_megapixel(mega_idx++, &UNUSED_HEADER);
            write_megapixel(mega_idx++, &UNUSED_HEADER);
            write_megapixel(mega_idx++, &UNUSED_HEADER);
            write_megapixel(mega_idx++, &UNUSED_HEADER);
            write_megapixel(mega_idx++, &UNUSED_HEADER);
            write_megapixel(mega_idx++, &UNUSED_HEADER);
            write_megapixel(mega_idx++, &UNUSED_HEADER);
            write_megapixel(mega_idx++, &UNUSED_HEADER);
        } else {
            write_megapixel(mega_idx++, &USED_HEADER);
            write_megapixel(mega_idx++, &USED_HEADER);
            write_megapixel(mega_idx++, &USED_HEADER);
            write_megapixel(mega_idx++, &USED_HEADER);
            write_megapixel(mega_idx++, &USED_DATA);
            write_megapixel(mega_idx++, &USED_DATA);
            write_megapixel(mega_idx++, &USED_DATA);
            write_megapixel(mega_idx++, &USED_DATA);
        }
        midx_t size = ((next - cur) &  HEAP_SIZE_MASK) * 8;
        size -= 8;
        while (size--) {
            write_megapixel(mega_idx++,
                    free_flg ? &UNUSED_DATA : &USED_DATA);
        }
        cur = next;
    } while(cur);

    // make filename
    strncpy(img_filename, img_path, MAX_PATH_LEN + 1);
    snprintf(img_filename, MAX_PATH_LEN + 1, "%s/%05d.png",
            img_path, img_idx);

    save_png();
    ++img_idx;
}

/****************************************
 * Cleanup
 ****************************************/
void closegraph(void) {
    assert(img.pixels);
    free(img.pixels);
}
#endif
// vim: set et sw=4 ts=4:
