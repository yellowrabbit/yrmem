#include <stdio.h>
#include <stdint.h>

#include "dbg.h"
#include "graph.h"
#include "yrmem.h"

void *ptr[4096 / 8] = { NULL };

int main(int argc, char *argv[]) {
    INITGRAPH(640, 640, "/tmp/");

    yrmem_init();
    yrmem_print_all();
    WRITE_GRAPH_MAP();

    mem_result_t res __attribute__((unused));

  /*
    yrmem_alloc(&ptr[0], 50);
    yrmem_alloc(&ptr[1], 50);
    yrmem_alloc(&ptr[2], 50);
    yrmem_alloc(&ptr[3], 50);
    yrmem_alloc(&ptr[4], 50);

    yrmem_print_all();

    yrmem_free(ptr[2]);
    yrmem_print_all();

    yrmem_init();
    yrmem_alloc(&ptr[0], 50);
    yrmem_alloc(&ptr[1], 50);
    yrmem_alloc(&ptr[2], 50);
    yrmem_alloc(&ptr[3], 50);
    yrmem_alloc(&ptr[4], 50);
    yrmem_print_all();
    yrmem_free(ptr[1]);
    yrmem_print_all();

    yrmem_init();
    yrmem_alloc(&ptr[0], 50);
    yrmem_alloc(&ptr[1], 50);
    yrmem_alloc(&ptr[2], 50);
    yrmem_alloc(&ptr[3], 50);
    yrmem_alloc(&ptr[4], 50);
    yrmem_print_all();
    yrmem_free(ptr[2]);
    yrmem_free(ptr[1]);
    yrmem_print_all();
*/
    /*
    yrmem_init();
    yrmem_alloc(&ptr[0], 50);
    yrmem_alloc(&ptr[1], 50);
    yrmem_alloc(&ptr[2], 50);
    yrmem_alloc(&ptr[3], 50);
    yrmem_alloc(&ptr[4], 50);
    yrmem_print_all();
    yrmem_free(ptr[1]);
    yrmem_print_all();
    yrmem_free(ptr[3]);
    yrmem_print_all();
    yrmem_free(ptr[2]);
    yrmem_print_all();
*/
    for (uint16_t i = 0; i < 4096 / 8; ++i) {
        res = yrmem_alloc(&ptr[i], 1 + (rand() % 99));
        if (res != MEM_OK) {
            ptr[i] = NULL;
        }
        yrmem_print_all();
        WRITE_GRAPH_MAP();

        for (uint f = 0; f < (uint)(rand() % 10); ++f) {
            uint16_t fr_idx = rand() % (i + 1);
            if (ptr[fr_idx]) {
                yrmem_free(ptr[fr_idx]);
                ptr[fr_idx] = NULL;
                yrmem_print_all();
                WRITE_GRAPH_MAP();
            }
        }
    }

    for (uint i = 0; i < 4096 / 8; ++i) {
        if (ptr[i]) {
            yrmem_free(ptr[i]);
            WRITE_GRAPH_MAP();
        }
    }

    CLOSEGRAPH();
    return(1);
}
// vim: set expandtab: sw=4 ts=4:
