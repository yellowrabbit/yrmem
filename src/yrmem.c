
#include "dbg.h"

#include "yrmem.h"
#include "yrmem-priv.h"

/****************************************
 * The heap
 ****************************************/
mhdr_t heap[HEAP_SIZE] = { 0 };

#define ADDR_2IDX(_addr) ((((uint8_t*)(_addr)) - &heap[0].u.data) / sizeof(mhdr_t))
#define IDX_2ADDR(_idx) ((void *)(&heap[_idx].u.data))

#define GET_SIZE(_idx) \
    (((heap[_idx].next - (_idx)) & HEAP_SIZE_MASK) \
    * sizeof(mhdr_t) - sizeof(heap[0].u.fr.next) - sizeof(heap[0].u.fr.prev))

#define GET_SIZE_IN_CHUNKS(_idx) \
    ((heap[_idx].next - (_idx)) & HEAP_SIZE_MASK)

#define SIZE_2CHUNKS(_size) \
    (1 + ((_size + 0x3) >> 3))

#define GET_HDR(_idx)  (&heap[_idx])
#define GET_PREV(_idx) (&heap[heap[_idx].prev])
#define GET_NEXT(_idx) (&heap[heap[_idx].next])

/****************************************
 * Protect the lists' manipulations
 ****************************************/
static MUTEX_TYPE mem_mutex;

/****************************************
 * Init the memory subsystem
 * ---
 * chunk 0 is marked as used AND included
 * in the free list.
 * Create one big free block from the chunk 1
 ****************************************/
void yrmem_init(void) {
    MUTEX_CREATE(&mem_mutex);
    // chunk 0
    mhdr_t *cur = GET_HDR(0);
    cur->free_flg = 0;
    cur->prev = 1;
    cur->next = 1;
    cur->u.fr.next = 1;
    cur->u.fr.prev = 1;
    // big free block
    cur = GET_HDR(1);
    cur->free_flg = 1;
    cur->next = 0;
    cur->prev = 0;
    cur->u.fr.next = 0;
    cur->u.fr.prev = 0;
}

/****************************************
 * Allocate block.
 * ptr - address of pointer to allocated data
 * size - size in bytes
 ****************************************/
mem_result_t yrmem_alloc(void **ptr, uint32_t size) {
    midx_t idx;
    mem_result_t res = yrmem_alloc_idx(&idx, size);
    *ptr = IDX_2ADDR(idx);
    return(res);
}

/****************************************
 * Allocate block.
 * idx --- address of the allocated block index
 * size --- size in bytes
 * Use first block >= size
 ****************************************/
mem_result_t yrmem_alloc_idx(midx_t *idx, uint32_t size) {
    assert(size);
    if (!MUTEX_GET(&mem_mutex)) {
        return(MEM_MUTEX);
    }

    midx_t size_chunks =  SIZE_2CHUNKS(size);
    dbg("size:%u, chunks:%u\n", size, size_chunks);

    midx_t cur = heap[0].u.fr.next;     // 0 is the free heap head always

    dbg("searching...\n");
    while (cur) { // ! head
        dbg("%d, size in chunks:%d\n", cur, GET_SIZE_IN_CHUNKS(cur));
        if (size_chunks <= GET_SIZE_IN_CHUNKS(cur)) {
            break;
        }
        cur = heap[cur].u.fr.next;
    }
    if (!cur) {
        dbg("  !!!! MEMORY FULL !!!!\n");
        MUTEX_RELEASE(&mem_mutex);
        return(MEM_FULL);
    }

    if (size_chunks < GET_SIZE_IN_CHUNKS(cur)) {
        // found, split the block
        midx_t new_block_idx = (heap[cur].next - size_chunks) & HEAP_SIZE_MASK;
        mhdr_t *new_block = GET_HDR(new_block_idx);
        new_block->free_flg = 0;
        new_block->next = heap[cur].next;
        new_block->prev = cur;
        heap[cur].next = new_block_idx;
        heap[new_block->next].prev = new_block_idx;
        *idx = new_block_idx;
    } else {
        // use full block
        heap[cur].free_flg = 0;
        midx_t free_prev_idx = heap[cur].u.fr.prev;
        midx_t free_next_idx = heap[cur].u.fr.next;
        heap[free_prev_idx].u.fr.next = heap[cur].u.fr.next;
        heap[free_next_idx].u.fr.prev = heap[cur].u.fr.prev;
        *idx = cur;
    }

    MUTEX_RELEASE(&mem_mutex);
    return(MEM_OK);
}

/****************************************
 * Find the previous free block
 ****************************************/
static midx_t find_prev_free(midx_t idx);
static midx_t find_prev_free(midx_t idx) {
    do {
        idx = heap[idx].prev;
        if (heap[idx].free_flg) {
            break;
        }
    } while (idx); // 0 block is always free (and always used)
    return(idx);
}

/****************************************
 * Free block by address
 ****************************************/
mem_result_t yrmem_free(void *ptr) {
    return(yrmem_free_idx(ADDR_2IDX(ptr)));
}


/****************************************
 * Free block by index
 ****************************************/
mem_result_t yrmem_free_idx(midx_t idx) {
    assert(!heap[idx].free_flg);
    assert(idx);

    dbg("free idx:%d\n", idx);

    if (!MUTEX_GET(&mem_mutex)) {
        return(MEM_MUTEX);
    }

    mhdr_t *cur  = GET_HDR(idx);
    mhdr_t *prev = GET_PREV(idx);
    mhdr_t *next = GET_NEXT(idx);
    if (prev->free_flg) {
        // merge with previous free block. No need to modify the free list
        prev->next = cur->next;
        next->prev = cur->prev;
        // make the new merged block current
        idx = cur->prev;
        cur = GET_HDR(idx);
        // merge with next free block
        next = GET_NEXT(idx);
        if (next->free_flg) {
            // main list
            GET_HDR(next->next)->prev = idx;
            cur->next = next->next;
            // free list
            cur->u.fr.next = next->u.fr.next;
            GET_HDR(cur->u.fr.next)->u.fr.prev = idx;
        }
    } else if (next->free_flg) {
        // merge with next free block. Change both lists.
        // the main list first
        GET_HDR(next->next)->prev = idx;
        cur->next = next->next;
        // then the free list
        cur->u.fr.next = next->u.fr.next;
        cur->u.fr.prev = next->u.fr.prev;
        GET_HDR(cur->u.fr.next)->u.fr.prev = idx;
        GET_HDR(cur->u.fr.prev)->u.fr.next = idx;
        cur->free_flg = 1;
    } else {
        // no merge, just mark as free and include to the free list
        midx_t prev_idx = find_prev_free(idx);
        prev = GET_HDR(prev_idx);
        next = GET_HDR(prev->u.fr.next);
        cur->u.fr.next = prev->u.fr.next;
        cur->u.fr.prev = prev_idx;
        prev->u.fr.next = idx;
        next->u.fr.prev = idx;
        cur->free_flg = 1;
    }
    MUTEX_RELEASE(&mem_mutex);
    return(MEM_OK);
}

/****************************************
 * The debug routines
 * Shows all blocks.
 ****************************************/
void yrmem_print_all(void) {
    midx_t cur = 0;
    do {
        dbg("%4d %c [%d %d] ", cur, heap[cur].free_flg ? 'f' : ' ',
                heap[cur].next, heap[cur].prev);
        if (heap[cur].free_flg || (!cur)) {
            dbg("[%d %d] ", heap[cur].u.fr.next, heap[cur].u.fr.prev);
        }
        dbg("%lu\n", GET_SIZE(cur));
        cur = heap[cur].next;
    } while (cur);
}

/****************************************
 * For heap debug
 ****************************************/
mhdr_t const *priv_yrmem_get_heap(void) {
    return(heap);
}

// vim: set et sw=4 ts=4:
