/****************************************
 * OS-specific debug messages
 ****************************************/
#ifndef DBG_H
#define DBG_H

#ifndef NO_DBG
#ifdef __DragonFly__
#include <stdio.h>
#define dbg(_str, ...) \
    fprintf(stderr, _str, ##__VA_ARGS__)
#else
    // esp8266 :)
#define dbg(_str, ...) \
    os_printf(_str, ##__VA_ARGS__)
#endif // __DragonFly__
#else
#define dbg(_str)
#endif // NO_DBG

#endif // DBG_H
// vim: expandtab: sw=4 ts=4:
