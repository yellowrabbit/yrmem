/****************************************
 * YR tiny memory allocator
 ****************************************/
#ifndef YRMEM_PRIV_H
#define YRMEM_PRIV_H

#include <stdint.h>

#include "yrmem.h"

/****************************************
 * Every allocated (and free) block has
 * a little header at the beginning.
 * It links the blocks together and has
 * the used/free flag.
 * ---
 * The heap is aligned on 4 byte boundary and
 * ``divided'' into 8-byte chunks. Each chunk
 * may contain data or block header or be unused.
 * All blocks are in the main doubly linked list by
 * the prev and next fields of the header.
 * These fields contain indices of the chunks
 * (used or free).
 * Free blocks besides this are in another
 * doubly linked list by the fr.prev and fr.next
 * fields.
 * ---
 * The block size is not stored, instead the difference
 * between the indices of serial blocks in the main list
 * is used. It will work even in the case of the
 * last block when the index of the next block is 0:
 * (0 - last_block_index) & HEAP_SIZE_MASK :)
 * only when size of the heap is 2^n
 * ---
 * Block 0 is marked as used, but it also
 * is placed in a list of free blocks, which
 * allows one to use it as a well known
 * entry point in the list of free blocks.
 * In addition, the fact that it is marked as used,
 * prevents the merging of free blocks
 * at the beginning and end of heap.
 ****************************************/
typedef struct __attribute__ ((aligned(4))) __attribute__ ((packed)) {
    midx_t next: 15;      // next (used of free) block index
    midx_t free_flg: 1;   // 0 --- used block, 1 --- free block
    midx_t prev;          // prev (used or free) block index
    union {
        struct {
            midx_t next;  // next free block
            midx_t prev;  // prev free block
        } fr;
        uint8_t data;     // start of the data
    } u;
} mhdr_t;

mhdr_t const *priv_yrmem_get_heap(void);

#endif // YRMEM_PRIV_H
// vim: set et sw=4 ts=4:
