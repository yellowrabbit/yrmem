#ifndef YRMEM_H
#define YRMEM_H

#include <stdint.h>

#include "assert.h"

/****************************************
 * One can set own HEAP_SIZE and
 * HEAP_SIZE_MASK.
 * XXX If HEAP_SIZE_MASK undefined then the slower
 * calculaton of the block size will be used.
 ****************************************/
// 2^n chunks (each chunk is 8 byte wide)
#define HEAP_SIZE       512 // 0x200 ~4K heap
#define HEAP_SIZE_MASK 0x1ff

/****************************************
 * the type of the heap chunk index
 ****************************************/
typedef uint16_t midx_t;

/****************************************
 * Shared access primitives
 ****************************************/
#define MUTEX_TYPE uint8_t
#define MUTEX_CREATE(_mutex) \
    do { \
        *(_mutex) = 1; \
    } while (0)
#define MUTEX_GET(_mutex) \
    ( *(_mutex) == 1 ? *(_mutex) = 0, 1 : 0)
#define MUTEX_RELEASE(_mutex) \
    do { \
        assert(*(_mutex) == 0); \
        *(_mutex) = 1; \
    } while (0)


/****************************************
 * These are recoverable errors, if something
 * reaaly bad happened then the system just
 * crash.
 ****************************************/
typedef enum {
    MEM_OK = 0,
    MEM_MUTEX,
    MEM_FULL
} mem_result_t;

/****************************************
 * Reset all
 ****************************************/
void yrmem_init(void);

/****************************************
 * Allocate block
 * ptr --- address of pointer to
 *         the allocated data
 * size --- size in bytes
 ****************************************/
mem_result_t yrmem_alloc(void **ptr, uint32_t size);
mem_result_t yrmem_alloc_idx(midx_t *idx, uint32_t size);

/****************************************
 * Free block
 ****************************************/
mem_result_t yrmem_free(void *ptr);
mem_result_t yrmem_free_idx(midx_t idx);

/****************************************
 * The debug routines
 ****************************************/
void yrmem_print_all(void);

#endif // YRMEM_H
// vim: set et sw=4 ts=4:
