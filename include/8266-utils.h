#ifndef UTILS_H
#define UTILS_H

/****************************************
 * printf functions for esp8266
 ****************************************/

#define ALIGN4 __attribute__((aligned(4)))
#define RODATA __attribute__((section(".irom.text"))) ALIGN4
#define ROSTR __attribute__((section(".irom.strings"))) ALIGN4

int ets_sprintf(char *str, char const *format, ...)  __attribute__ ((format (printf, 2, 3)));
int os_printf_plus(char const *format, ...)  __attribute__ ((format (printf, 1, 2)));
int ets_snprintf(char *str, unsigned int size, char const *format, ...)  __attribute__ ((format (printf, 3, 4)));

#define os_sprintf_plus  ets_sprintf
#define os_sprintf(buf, fmt, ...) os_sprintf_plus(buf, fmt, ##__VA_ARGS__)

#define os_snprintf_plus  ets_snprintf
#define os_snprintf(buf, size, fmt, ...) do { \
        static const char flash_str[] ROSTR = fmt;  \
        os_snprintf_plus(buf, size, flash_str, ##__VA_ARGS__); \
    } while(0)

#define os_printf(fmt, ...) do {    \
        static char const flash_str[] ROSTR = fmt;  \
        os_printf_plus(flash_str, ##__VA_ARGS__);   \
    } while(0)

#endif // UTILS_H
// vim: expandtab:sw=4 ts=4:
